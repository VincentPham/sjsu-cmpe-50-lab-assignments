//============================================================================
// Name        : Lab_2_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Lab 2, Function Overloading, Arrays & Multi-Dimensional Arrays
//============================================================================

#include <iostream>
using namespace std;

#define HOMEWORK 0
#define LAB 1

int Add(int a, int b);
int Add(int a, int b, int c);
double Add(int y, double d);
double Add(int x, double e, double f);

void function_overload(void);
void _array(void);
void grade_input(void);

void best_worst_grade(double arr[][8], int grade_type);
void get_average(double arr[][8], int grade_type);

void grade_UI(double grades[][8]);

int main()
{

//Uncomment the following line for Exercise 1
	//function_overload();

//Uncomment the following line for Exercise 2
	//_array();

//Uncomment the follwing line for Exercise 3
	grade_input();

}

void grade_input(void)
{
	double grades[2][8] = {{0},{0}};

	cout << "Please enter your grades for the class." << endl;
	cout << "NOTE: To pass the class, you must receive a percentage of 50% or better in homework AND LABS." << endl;
	cout << endl;

	for(int i = 0; i < 5; i++)
	{
		cout << "Please enter in homework grade #" << i+1 << endl;
		cin >> grades[0][i];
	}

	for(int i = 0; i < 5; i++)
	{
		cout << "Please enter in lab grade #" << i+1 << endl;
		cin >> grades[1][i];
	}

	best_worst_grade(grades, HOMEWORK);
	get_average(grades, HOMEWORK);

	best_worst_grade(grades,LAB);
	get_average(grades,LAB);

	grade_UI(grades);

}


void grade_UI(double grades[][8])
{
	double sum = 0;

	//UI
		cout << "\t1\t2\t3\t4\t5" << endl;
		cout << "\t*********************************" << endl;

		//HOMEWORK OUTPUT
		cout << "HW";
		cout.width(7);
		for(int x = 0; x < 5; x++)
		{
			cout << grades[HOMEWORK][x];
			cout.width(8);
		}
		cout << "\t BEST HW: " << grades[HOMEWORK][5];
		cout.width(8);
		cout << "\t WORST HW: " << grades[HOMEWORK][6];
		cout.width(8);
		cout << "\t AVG HW: " << grades[HOMEWORK][7];
		cout << endl;

		//LAB OUTPUT
		cout.width(0);
		cout << "LAB";
		cout.width(6);
		for(int x = 0; x < 5; x++)
		{
			cout << grades[LAB][x];
			cout.width(8);
		}
		cout << "\t BEST LAB: " << grades[LAB][5];
		cout.width(8);
		cout << "\t WORST LAB: " << grades[LAB][6];
		cout.width(8);
		cout << "\t AVG LAB: " << grades[LAB][7];
		cout << endl << endl;

		//AVG OUTPUT
		cout.width(0);
		cout << "AVG";
		cout.width(6);
		for(int y = 0; y < 5; y++)
		{
			sum += ((grades[HOMEWORK][y] + grades[LAB][y]) / 2);
			cout << ((grades[HOMEWORK][y] + grades[LAB][y]) / 2);
			cout.width(8);
		}
		cout << "\t OVERALL AVG: " << (sum/5) << endl;
		cout << endl;

		//Pass or Fail Analysis
		if((grades[HOMEWORK][7] < 50) || (grades[LAB][7] < 50))
		{
			cout << "I am sorry to inform you that you have failed the class, "
					"because you have received an average" << endl
					<< "of less than 50% in either homeworks or labs." << endl << endl;
		}
		else
		{
			cout << "Congratulations! You have passed the class!" << endl;
		}

}

//Function finds min and max grades
void best_worst_grade(double arr[][8], int grade_type)
{
		//Finding max in min score
		double max = arr[grade_type][0];
		double min = max;

		for(int i= 1; i < 5; i++)
		{
			//Finds max
			if(max < arr[grade_type][i])
			{
				max = arr[grade_type][i];
			}

			//Finds min
			if(min > arr[grade_type][i])
			{
				min = arr[grade_type][i];
			}
		}

		arr[grade_type][5] = max;
		arr[grade_type][6] = min;
}

void get_average(double arr[][8], int grade_type)
{//Gets average
	double average = 0, sum = 0;

	for(int i = 0; i < 5; i++)
	{
		sum += arr[grade_type][i];
	}

	average = sum/5;

	arr[grade_type][7] = average;
}


void function_overload(void)
{
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	int sum1, sum2;
	double sum3, sum4;

	sum1 = Add(2, 3); //5
	sum2 = Add(2, 3, 4); //9
	sum3 = Add(2, 3.0); //5.00
	sum4 = Add(2, 3.0, 5.0); //10.00

	cout << sum1 << endl;
	cout << sum2 << endl;
	cout << sum3 << endl;
	cout << sum4 << endl;
}

void _array(void)
{
	char _arr[100] = {0};
	cout << "Please enter no more than 10 characters. Press enter after each character." << endl;
	cout << "When you are finished enter a '.' at the end." << endl;
	int i = 0;

	do
	{
		cin >> _arr[i];
		i++;

	}while(_arr[i-1] != '.');

	for(int j = i-2; j >= 0; j--)
	{
		cout << _arr[j];
	}
	cout << endl;
	cout << "Array size is " << i-1 << endl;
}


int Add(int a, int b)
{
	return (a+b);
}

int Add(int a, int b, int c)
{
	return a+b+c;
}

double Add(int y, double d)
{
	return (double)y+d;
}

double Add(int x, double e, double f)
{
	return x+e+f;
}
