//============================================================================
// Name        : Lab_5_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Lab 5 - CMPE50 - Classes
//============================================================================

#include <iostream>
using namespace std;

void exercise_1();
void exercise_2();

class student_record
{
public:
	student_record(); //Default constructor
	student_record(int arr_quiz[], int midterm, int final);
	void set_quizScore(int quiz1, int quiz2);
	void set_midtermScore(int midterm);
	void set_finalScore(int final);
	void calculateAverage();
	int* get_quizScore();
	int get_midtermScore();
	int get_finalScore();
	double get_Average();
	~student_record();
private:
	int quiz_score[2];
	int midterm_score;
	int final_score;
	double weighted_average;
};

class CounterType
{
public:
	CounterType(); //Default Constructor
	CounterType(int count_val);
	void increase_count();
	void decrease_count();
	int get_count();
	void output_count(ostream& outs);

private:
	int count;
};

int main()
{

//Uncomment the following line for exericse 1
	exercise_1();

//Uncomment the following line for exericse 2
	//exercise_2();

}

//*****CLASS DEFINITION: student_record*****//
student_record::student_record()
{//Default Constructor
	quiz_score[0] = 0;
	quiz_score[1] = 0;
	midterm_score = 0;
	final_score = 0;
	weighted_average = 0;
}

student_record::student_record(int arr_quiz[], int midterm, int final)
{//Constructor, takes in quiz scores store in array
	quiz_score[0] = arr_quiz[0];
	quiz_score[1] = arr_quiz[1];
	midterm_score = midterm;
	final_score = final;
	calculateAverage();
}

student_record::~student_record()
{//destructor
	cout << "The class has been destructed!" << endl;
}
void student_record::set_quizScore(int quiz1, int quiz2)
{//Mutator function, sets quiz scores
	quiz_score[0] = quiz1;
	quiz_score[1] = quiz2;
}

void student_record::set_midtermScore(int midterm)
{//Mutator, sets midterm score
	midterm_score = midterm;
}

void student_record::set_finalScore(int final)
{//Mutator, sets final score
	final_score = final;
}

void student_record::calculateAverage()
{
	weighted_average = ((((float)quiz_score[0] + quiz_score[1]) / 20) * 100 * 0.25) + (midterm_score * 0.25) + (final_score * 0.5);
}

int* student_record::get_quizScore()
{//Accessor function
	return quiz_score;
	//returns starting address of member array, array_quizscore[]
}

int student_record::get_midtermScore()
{//Accessor function
	return midterm_score;
}

int student_record::get_finalScore()
{//Accessor function
	return final_score;
}

double student_record::get_Average()
{//Accessor function
	return weighted_average;
}

//*****CLASS DEFINITION: CounterType*****//
CounterType::CounterType()
{//Constructor
	count = 0;
}

CounterType::CounterType(int count_val)
{//Constructor
	count = count_val;
}

void CounterType::increase_count()
{
	count++;
}

void CounterType::decrease_count()
{
	if(count == 0)
	{
		cout << "The counter cannot be decremented." << endl;
		cout << "The counter is already at 0." << endl;
		return;
	}

	if (count >= 0)
	{
		count--;
		if(count == 0)
		{
			cout << "The counter has reached 0." << endl;
		}
	}

}

int CounterType::get_count()
{//Accessor function
	return count;
}

void CounterType::output_count(ostream& outs)
{
	outs << "The counter value is " << count << endl;
}

void exercise_1()
{
	int quiz_store[2] = {9, 10};
	student_record student_1, student_2(quiz_store, 100, 100);
	int* quiz_student1;
	int* quiz_student2;

	cout << "STUDENT 1" << endl;
	quiz_student1 = student_1.get_quizScore();
	cout << "Quiz 1: " << *(quiz_student1) << endl << "Quiz 2: " << *(quiz_student1 + 1) << endl;
	cout << "Midterm score: " << student_1.get_midtermScore() << endl;
	cout << "Final score: " << student_1.get_finalScore() << endl;
	cout << "Weighted Average: " << student_1.get_Average() << endl;

	cout << endl << "STUDENT 2" << endl;
	quiz_student2 = student_2.get_quizScore();
	cout << "Quiz 1: " << *(quiz_student2) << endl << "Quiz 2: " << *(quiz_student2 + 1) << endl;
	cout << "Midterm score: " << student_2.get_midtermScore() << endl;
	cout << "Final score: " << student_2.get_finalScore() << endl;
	cout << "Weighted Average: " << student_2.get_Average() << endl;

	cout << endl << "STUDENT 3" << endl;
	student_1.set_finalScore(85);
	student_1.set_midtermScore(95);
	student_1.set_quizScore(10, 7);
	student_1.calculateAverage();
	cout << "Quiz 1: " << *(quiz_student1) << endl << "Quiz 2: " << *(quiz_student1 + 1) << endl;
	cout << "Midterm score: " << student_1.get_midtermScore() << endl;
	cout << "Final score: " << student_1.get_finalScore() << endl;
	cout << "Weighted Average: " << student_1.get_Average() << endl;
}

void exercise_2()
{
	CounterType count1, count2(100);

	cout << "Count 1: ";
	count1.output_count(cout);
	for(int i = 0; i < 1001; i++)
	{
		count1.increase_count();
	}
	count1.output_count(cout);
	cout << endl;

	cout << count1.get_count() << endl;

	cout << "Count 2: ";
	count2.output_count(cout);
	for(int i = 0; i < 100; i++)
	{
		count2.decrease_count();
	}
	count2.decrease_count();
	count2.output_count(cout);

	cout << count2.get_count() << endl;
}

