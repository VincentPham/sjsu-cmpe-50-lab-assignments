/*
 * polynomial.cpp
 *
 */

#include <iostream>
#include <ostream>
#include <cmath>
#include "polynomial.h"
using namespace std;


Polynomial::Polynomial()
{
	degree = 10;
	coeff = new double[degree+1];
	for (int i = 0; i <= degree; i++)
	{
		coeff[i] = 0;
	}
}


Polynomial::Polynomial(int degr)
{
	degree = degr;
	coeff = new double[degree+1];
	for (int i = 0; i <= degree; i++)
	{
		coeff[i] = 0;
	}
}


/// Copy constructor
Polynomial::Polynomial(const Polynomial &poly)
{
	degree = poly.get_degree();
	coeff = new double[degree+1];
	for (int i = 0; i <= degree; i++)
	{
		coeff[i] = poly.get_coeff(i);
	}
}


Polynomial::Polynomial(double cf[], int deg)
{
	degree = deg;
	coeff = new double[degree+1];
	for (int i = degree; i >= 0; i--)
	{
		coeff[i] = cf[degree - i];
	}
}


Polynomial::~Polynomial()
{
	delete [] coeff;

}


int Polynomial::get_degree() const
{
	return degree;
}


double Polynomial::get_coeff(int deg) const
{
	if (degree < deg)
	{
		return 0;
		// The input degree is larger than the polynomial degree
	}
	return coeff[deg];
}


void Polynomial::set_coeff(int degr, double val)
{
	if (degree < degr)
	{
		cout << "Degree exceeded." << endl;
		return;
	}
	coeff[degr] = val;
}


/// Evaluate the polynomial
double Polynomial::evaluate(double val)
{
	double result = 0;

	for(int i = degree; i >= 0; i--)
	{
		result += (coeff[i]*pow(val, i));
	}

	return result;
}


Polynomial& Polynomial::operator =(const Polynomial &poly)
{
	if (this == &poly)
	{
		// Copy to itself. Nothing to be done.
		return *this;
	}

	degree = poly.degree;

	delete [] coeff;

	coeff = new double[degree+1];

	for(int i = 0; i < degree + 1; i++)
	{
		coeff[i] = poly.coeff[i];
	}

	return *this;
}


Polynomial operator+(const Polynomial &pola, const Polynomial &polb)
{
	int new_degree = 0;

	new_degree = (pola.degree > polb.degree) ? pola.degree : polb.degree;
	Polynomial polSum(new_degree);
	int itr = 0;
	if(pola.degree > polb.degree)
	{
		int diff = pola.degree - polb.degree;
		int i = 0;
		for(; i < diff; i++)
		{
			polSum.coeff[new_degree - i] = pola.coeff[new_degree - i];
		}

		itr = new_degree - i;
	}
	else if(polb.degree > pola.degree)
	{
		int diff = polb.degree - pola.degree;
		int i = 0;
		for(; i < diff; i++)
		{
			polSum.coeff[new_degree - i] = polb.coeff[new_degree - i];
		}

		itr = new_degree - i;
	}
	else
	{
		itr = new_degree;
	}

	for(;itr >= 0; itr--)
	{
		polSum.coeff[itr] = pola.coeff[itr] + polb.coeff[itr];
	}

	return polSum;
}


Polynomial operator+(const double constant, const Polynomial &polb)
{
	Polynomial polSum(polb);

	polSum.coeff[0] = constant + polb.coeff[0];

	return polSum;
}


Polynomial operator+(const Polynomial &pola, const double constant)
{
	Polynomial polSum(pola);

	polSum.coeff[0] = constant + pola.coeff[0];

	return polSum;
}


Polynomial operator-(const Polynomial &pola, const Polynomial &polb)
{

	/// Convert polb to -polb and add to pola to simplify subtraction
	Polynomial neg_polb(polb);
	int itr = neg_polb.degree;
	for(;itr >= 0; itr--)
	{
		neg_polb.coeff[itr] = neg_polb.coeff[itr] * (-1);
	}

	int new_degree = 0;
	new_degree = (pola.degree > neg_polb.degree) ? pola.degree : neg_polb.degree;

	/// Create new Polynomial object to store result
	Polynomial polSub(new_degree);

	if(pola.degree > neg_polb.degree)
	{
		int diff = pola.degree - neg_polb.degree;
		int i = 0;
		for(; i < diff; i++)
		{
			polSub.coeff[new_degree - i] = pola.coeff[new_degree - i];
		}

		itr = new_degree - i;
	}
	else if(neg_polb.degree > pola.degree)
	{
		int diff = neg_polb.degree - pola.degree;
		int i = 0;
		for(; i < diff; i++)
		{
			polSub.coeff[new_degree - i] = neg_polb.coeff[new_degree - i];
		}

		itr = new_degree - i;
	}
	else
	{
		itr = new_degree;
	}

	for(;itr >= 0; itr--)
	{
		polSub.coeff[itr] = pola.coeff[itr] + neg_polb.coeff[itr];
	}

	return polSub;

}


Polynomial operator-(const double constant, const Polynomial &polb)
{
	Polynomial polSub(polb);

	int itr = polb.degree;
	for(;itr > 0; itr--)
	{
		polSub.coeff[itr] = polb.coeff[itr] * (-1);
	}

	polSub.coeff[0] = constant - polb.coeff[0];

	return polSub;
}


Polynomial operator-(const Polynomial &pola, const double constant)
{
	Polynomial polSub(pola);

	polSub.coeff[0] = pola.coeff[0] - constant;

	return polSub;
}


Polynomial operator*(const Polynomial &pola, const Polynomial &polb)
{
	int new_degree = pola.get_degree() + polb.get_degree();

	Polynomial polProd(new_degree);

	for(int degA = pola.get_degree(); degA >= 0; degA--)
	{
		for(int degB = polb.get_degree(); degB >= 0; degB--)
		{
			cout << "degA + degB = " << degA + degB << endl;
			cout << "pola coeff [" << degA << "] = " << pola.get_coeff(degA) << endl;
			cout << "polb coeff [" << degB << "] = " << polb.get_coeff(degB) << endl;


			polProd.coeff[degA + degB] += (pola.get_coeff(degA) * polb.get_coeff(degB));
		}
	}

	return polProd;
}


Polynomial operator*(const double constant, const Polynomial &polb)
{
	Polynomial polProd(polb);

	int itr = polb.degree;
	for(;itr >= 0; itr--)
	{
		polProd.coeff[itr] = polb.coeff[itr] * constant;
	}

	return polProd;
}


Polynomial operator*(const Polynomial &pola, const double constant)
{
	Polynomial polProd(pola);

	int itr = pola.degree;
	for(;itr >= 0; itr--)
	{
		polProd.coeff[itr] = pola.coeff[itr] * constant;
	}

	return polProd;

}


ostream& operator<<(ostream& ost, const Polynomial &pol)
{
	double coeff = 0;

	int i = pol.get_degree();
	coeff = pol.get_coeff(i);
	ost << coeff << "x^" << i;
	i--;

	for(; i > 0; i--)
	{
		coeff = pol.get_coeff(i);

		if(coeff >= 0)
		{
			ost << "+";
		}
		ost << coeff << "x^" << i;

	}

	if(pol.get_coeff(0) >= 0)
	{
		ost << "+";
	}
	ost << pol.get_coeff(0) << endl;

	return ost;
}


istream& operator>>(istream& ist, Polynomial &pol)
{

	cout << "Enter the polynomial degree." << endl;
	ist >> pol.degree;
	cout << "Entered degree = " << pol.degree << endl;

	pol.coeff = new double[pol.degree + 1];

	for (int i = pol.degree; i >= 0; i--)
	{
		cout << "Enter degree " << i << " coefficient" << endl;

		ist >> pol.coeff[i];
	}

	return ist;

}


bool operator==(const Polynomial &lhs, const Polynomial &rhs)
{

	bool equals = false;

	if(lhs.degree == rhs.degree)
	{
		for(int i = 0; i < lhs.degree + 1; i ++)
		{
			if(lhs.coeff[i] != rhs.coeff[i])
			{
				equals = false;
				return equals;
			}
		}

		equals = true;
	}

	return equals;
}
