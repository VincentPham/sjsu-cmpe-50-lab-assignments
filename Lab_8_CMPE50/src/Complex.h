/*
 * Complex.h
 * Description: Complex class interface file
 */
#ifndef COMPLEX_H_
#define COMPLEX_H_

#include <iostream>
using namespace std;

/// imaginary_t structure to be used with the Complex class
struct imaginary_t
{
	imaginary_t():value(0)
	{

	}

	imaginary_t(double val)
	{
		value = val;
	}

	imaginary_t(int val)
	{
		value = (double)val;
	}

	imaginary_t operator=(const imaginary_t imaginary)
	{
		this->value = imaginary.value;
		return *this;
	}

	imaginary_t operator=(const double val)
	{
		this->value = val;

		return *this;
	}

	imaginary_t operator=(const int val)
	{
		this->value = (double)val;

		return *this;
	}

	friend bool operator==(const imaginary_t lhs, const imaginary_t rhs)
	{
		bool equals = false;

		if(lhs.value == rhs.value)
		{
			equals = true;
		}

		return equals;
	}

	double value;
};

/// real_t structure to be used with the Complex class
struct real_t
{
	real_t():value(0)
	{

	}

	real_t(double val)
	{
		value = val;
	}

	real_t(int val)
	{
		value = (double)val;
	}

	real_t operator=(const real_t real)
	{
		this->value = real.value;

		return *this;
	}

	real_t operator=(const double val)
	{
		this->value = val;

		return *this;
	}

	real_t operator=(const int val)
	{
		this->value = (double)val;

		return *this;
	}

	friend bool operator==(const real_t lhs, const real_t rhs)
	{
		bool equals = false;

		if(lhs.value == rhs.value)
		{
			equals = true;
		}

		return equals;
	}

	double value;
};


class Complex
{
public:

	Complex();
	Complex(double real_part, double imaginary_part);
	Complex(double real_part);
	Complex(const Complex &complex_num); ///< Copy constructor
	double get_real();
	double get_imaginary();

	/// Assignment operator overload
	Complex& operator=(const Complex &complex_num);

	/// Addition operator overloads
	friend Complex operator+(real_t real, Complex &complex_num);
	friend Complex operator+(Complex &complex_num, real_t real);
	friend Complex operator+(imaginary_t imaginary, Complex &complex_num);
	friend Complex operator+(Complex &complex_num, imaginary_t imaginary);
	friend Complex operator+(Complex &complex1, Complex &complex2);

	/// Subtraction operator overloads
	friend Complex operator-(real_t real, Complex &complex_num);
	friend Complex operator-(Complex &complex_num, real_t real);
	friend Complex operator-(imaginary_t imaginary, Complex &complex_num);
	friend Complex operator-(Complex &complex_num, imaginary_t imaginary);
	friend Complex operator-(Complex &complex1, Complex &complex2);

	/// Multiplication operator overloads
	friend Complex operator*(real_t real, Complex &complex_num);
	friend Complex operator*(Complex &complex_num, real_t real);
	friend Complex operator*(imaginary_t imaginary, Complex &complex_num);
	friend Complex operator*(Complex &complex_num, imaginary_t imaginary);
	friend Complex operator*(Complex &complex1, Complex &complex2);

	///Insertion operator overload
	friend ostream& operator<<(ostream& ost, const Complex &complex_num);

	///Extraction operator overload
	friend istream& operator>>(istream& ist, Complex &complex_num);

	///'Equal-to' operator overload
	friend bool operator==(Complex &lhs, Complex &rhs);

private:
	real_t real;			//< Real member
	imaginary_t imaginary;	//< Imaginary member

};

// Imaginary constant i
const Complex i(0,1);


#endif /* COMPLEX_H_ */
