/*
 * polynomial.h
 *
 */
#ifndef POLYNOMIAL_H_
#define POLYNOMIAL_H_

using namespace std;

class Polynomial
{

public:
	Polynomial(); ///< Default constructor
	Polynomial(int degr);
	Polynomial(const Polynomial &poly); ///< Copy constructor
	Polynomial(double cf[], int deg);

	/// Getter and setter functions
	int get_degree() const;
	double get_coeff(int deg) const;
	void set_coeff(int degr, double val);

	/// Function to evaluate polynomial
	double evaluate(double val);

	/// Assignment operator overload
	Polynomial& operator=(const Polynomial &poly);

	/// Addition operator overload
	friend Polynomial operator+(const Polynomial &pola, const Polynomial &polb);
	friend Polynomial operator+(const double constant, const Polynomial &polb);
	friend Polynomial operator+(const Polynomial &pola, const double constant);

	/// Subtraction operator overload
	friend Polynomial operator-(const Polynomial &pola, const Polynomial &polb);
	friend Polynomial operator-(const double constant, const Polynomial &polb);
	friend Polynomial operator-(const Polynomial &pola, const double constant);

	/// Multiplication operator overload
	friend Polynomial operator*(const Polynomial &pola, const Polynomial &polb);
	friend Polynomial operator*(const double constant, const Polynomial &polb);
	friend Polynomial operator*(const Polynomial &pola, const double constant);

	/// Insertion operator overload
	friend ostream& operator<<(ostream& ost, const Polynomial &pol);

	/// Extraction operator overload
	friend istream& operator>>(istream& ist, Polynomial &pol);

	/// 'Equal-to' operator overload
	friend bool operator==(const Polynomial &lhs, const Polynomial &rhs);

	/// Destructor
	~Polynomial();

private:
	int degree;
	double *coeff;

};



#endif /* POLYNOMIAL_H_ */
