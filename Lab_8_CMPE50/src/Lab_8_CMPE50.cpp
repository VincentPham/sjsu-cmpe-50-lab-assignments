//============================================================================
// Name        : Lab_8_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include "polynomial.h"
#include "Complex.h"

using namespace std;

void exercise_1();
void exercise_2();
void printPolynomial(Polynomial &poly);
void printComplex(Complex &complex_num);

int main()
{
	//Uncomment the following line for exercise 1
	//exercise_1();

	//Uncomment the following line for exercise 2
	exercise_2();
}


void exercise_1()
{
	double pol1[] = {-94, 3, 0, 2, 34};
	int deg_1 = (sizeof(pol1)/sizeof(double)) - 1;    ///< Get degree of polynomial

	double pol2[] = {100, -3, -2, -5, -6, 100, -8};
	int deg_2 = (sizeof(pol2)/sizeof(double)) - 1;    ///< Get degree of polynomial

	Polynomial poly1(pol1 , deg_1), poly2(pol2, deg_2);

	double constant1 = 100;
	double constant2 = -87;


	cout << "**********SUM0**********" << endl;
	Polynomial sum0;

	sum0 = poly1 + poly2; ///< Polynomial + Polynomial

	printPolynomial(poly1);
	cout << " + " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(sum0);

	cout << "**********SUM1**********" << endl;
	Polynomial sum1 = constant1 + poly1; ///< Constant + Polynomial

	cout << constant1 << endl;
	cout << " + " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(sum1);

	cout << "**********SUM2**********" << endl;
	Polynomial sum2 = constant2 + poly2;

	cout << constant2 << endl;
	cout << " + " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(sum2);

	cout << "**********SUM3**********" << endl;
	Polynomial sum3 = poly1 + constant1; ///< Polynomial + Constant

	printPolynomial(poly1);
	cout << " + " << endl;
	cout << constant1 << endl;
	cout << " = " << endl;
	printPolynomial(sum3);

	cout << "**********SUM4**********" << endl;
	Polynomial sum4 = poly1 + constant2;

	printPolynomial(poly1);
	cout << " + " << endl;
	cout << constant2 << endl;
	cout << " = " << endl;
	printPolynomial(sum4);

	cout << "**********SUB0**********" << endl;
	Polynomial sub0 = poly1 - poly2; ///< Polynomial - Polynomial

	printPolynomial(poly1);
	cout << " - " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(sub0);

	cout << "**********SUB1**********" << endl;
	Polynomial sub1 = constant1 - poly1; ///< Constant - Polynomial

	cout << constant1 << endl;
	cout << " - " << endl;
	printPolynomial(poly1);
	cout << " = " << endl;
	printPolynomial(sub1);

	cout << "**********SUB2**********" << endl;
	Polynomial sub2 = constant2 - poly1;

	cout << constant2 << endl;
	cout << " - " << endl;
	printPolynomial(poly1);
	cout << " = " << endl;
	printPolynomial(sub2);

	cout << "**********SUB3**********" << endl;
	Polynomial sub3 = poly1 - constant1; ///< Polynomial - Constant

	printPolynomial(poly1);
	cout << " - " << endl;
	cout << constant1 << endl;
	cout << " = " << endl;
	printPolynomial(sub3);

	cout << "**********SUB4**********" << endl;
	Polynomial sub4 = poly1 - constant2;

	printPolynomial(poly1);
	cout << " - " << endl;
	cout << constant2 << endl;
	cout << " = " << endl;
	printPolynomial(sub4);

	cout << "**********PROD0**********" << endl;
	Polynomial prod0 = poly1 * poly2; ///< Polynomial * Polynomial

	printPolynomial(poly1);
	cout << " * " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(prod0);

	cout << "**********PROD1**********" << endl;
	Polynomial prod1 = constant1 * poly1; ///< Constant * Polynomial

	cout << constant1 << endl;
	cout << " * " << endl;
	printPolynomial(poly2);
	cout << " = " << endl;
	printPolynomial(prod1);

	cout << "**********PROD2**********" << endl;
	Polynomial prod2 = poly2 * constant2; ///<Polynomial * Constant

	printPolynomial(poly2);
	cout << " * " << endl;
	cout << constant2 << endl;
	cout << " = " << endl;
	printPolynomial(prod2);

	cout << "**********ASSIGNMENT OPERATOR OVERLOAD TEST**********" << endl;
	Polynomial prod2_copy = prod2;
	cout << "prod2_copy: ";
	printPolynomial(prod2_copy);
	cout << prod2_copy << endl;

	Polynomial prod1_copy = prod1;
	cout << "prod1_copy: ";
	printPolynomial(prod1_copy);
	cout << prod1_copy << endl;


	cout << "**********INSERTION/EXTRACTION/COMPARISON OPERATOR OVERLOAD TEST**********" << endl;
	Polynomial poly_input, poly_input1;

	cout << "Please enter in a polynomial." << endl;
	cin >> poly_input;
	cout << "You entered in " << poly_input << endl;

	poly_input1 = poly_input;

	poly_input1 = poly_input1 + 1;

	cout << poly_input1 << endl;

	if(poly_input == poly_input1)
	{
		cout << "poly_input EQUALS poly_input1" << endl;
	}
	else if (!(poly_input == poly_input1))
	{
		cout << "poly_input DOES NOT EQUAL poly_input1" << endl;
	}

	Polynomial poly_input2, poly_input3;

	cout << "Please enter in a polynomial." << endl;
	cin >> poly_input2;
	cout << "You entered in " << poly_input2 << endl;

	poly_input3 = poly_input2;

	poly_input2 = poly_input3;

	if(poly_input2 == poly_input3)
	{
		cout << "poly_input EQUALS poly_input2" << endl;
	}
	else if (!(poly_input2 == poly_input3))
	{
		cout << "poly_input DOES NOT EQUAL poly_input3" << endl;
	}

}

void exercise_2()
{
	Complex complex1(12,12), complex2, result;

	cout << "Enter in a complex number" << endl;
	cin >> complex2;
	cout << "You entered " << complex2 << endl;


	cout << "**********SUM0**********" << endl;
	cout << "complex1 + complex2 = "<< complex1 + complex2 << endl;
	cout << "**********SUB0**********" << endl;
	cout << "complex1 + complex2 = "<<complex1 - complex2 << endl;
	cout << "**********PROD0**********" << endl;
	cout << " complex1 * complex2 = "<< complex1 * complex2 << endl;

	complex1 = complex2;

	cout << "complex1 = " << complex1 << endl;

	if(complex1 == complex2)
	{
		cout << "complex1 EQUALS complex2" << endl;
	}
	else if(!(complex1 == complex2))
	{
		cout << "complex1 DOES NOT EQUAL complex2" << endl;
	}

	complex1 = complex1 + (real_t)1;

	if(complex1 == complex2)
	{
		cout << "complex1 EQUALS complex2" << endl;
	}
	else if(!(complex1 == complex2))
	{
		cout << "complex1 DOES NOT EQUAL complex2" << endl;
	}
}


void printPolynomial(Polynomial &poly)
{
	double coeff = 0;

	int i = poly.get_degree();
	coeff = poly.get_coeff(i);
	cout << coeff << "x^" << i;
	i--;

	for(; i > 0; i--)
	{
		coeff = poly.get_coeff(i);

		if(coeff >= 0)
		{
			cout << "+";
		}
		cout << coeff << "x^" << i;

	}

	if(poly.get_coeff(0) >= 0)
	{
		cout << "+";
	}
	cout << poly.get_coeff(0) << endl;
}


void printComplex(Complex &complex_num)
{
	cout << complex_num.get_real();

	if(complex_num.get_imaginary() >= 0)
	{
		cout << "+";
	}

	cout << complex_num.get_imaginary() << "i";
}
