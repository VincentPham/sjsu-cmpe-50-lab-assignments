/*
 * Complex.cpp
 * Description: Complex class implementation file
 *
 */

#include "Complex.h"

#include <iostream>
#include <ostream>
#include <cmath>
using namespace std;


Complex::Complex():
		real(0),
		imaginary(0)
{

}


Complex::Complex(double real_part, double imaginary_part):
		real(real_part),
		imaginary(imaginary_part)
{

}


Complex::Complex(double real_part):
		real(real_part),
		imaginary(0)
{

}


/// Copy constructor
Complex::Complex(const Complex &complex_num)
{
	real = complex_num.real;
	imaginary = complex_num.imaginary;
}


double Complex::get_real()
{
	return real.value;
}


double Complex::get_imaginary()
{

	return imaginary.value;
}


Complex& Complex::operator =(const Complex &complex)
{
	if (this == &complex)
	{
		// Copy to itself. Nothing to be done.
		cout << "Self assignment, do nothing!" << endl;
		return *this;
	}

	real = complex.real;
	imaginary = complex.imaginary;

	return *this;
}

Complex operator+(real_t real, Complex &complex_num)
{
	Complex complexSum(complex_num);
	complexSum.real.value += real.value;
	return complexSum;
}


Complex operator+(Complex &complex_num, real_t real)
{
	Complex complexSum(complex_num);
	complexSum.real.value += real.value;
	return complexSum;
}


Complex operator+(imaginary_t imaginary, Complex &complex_num)
{
	Complex complexSum(complex_num);
	complexSum.imaginary.value += imaginary.value;
	return complexSum;
}


Complex operator+(Complex &complex_num, imaginary_t imaginary)
{
	Complex complexSum(complex_num);
	complexSum.imaginary.value += imaginary.value;
	return complexSum;
}


Complex operator+(Complex &complex1, Complex &complex2)
{
	Complex complexSum(complex1);
	complexSum.real.value += complex2.real.value;
	complexSum.imaginary.value += complex2.real.value;
	return complexSum;
}


Complex operator-(real_t real, Complex &complex_num)
{
	Complex complexSub(complex_num);
	complexSub.real.value *= (-1);
	complexSub.imaginary.value *= (-1);
	complexSub.real.value += real.value;

	return complexSub;
}


Complex operator-(Complex &complex_num, real_t real)
{
	Complex complexSub(complex_num);
	complexSub.real.value -= real.value;

	return complexSub;
}


Complex operator-(imaginary_t imaginary, Complex &complex_num)
{
	Complex complexSub(complex_num);
	complexSub.real.value *= (-1);
	complexSub.imaginary.value *= (-1);
	complexSub.imaginary.value += imaginary.value;

	return complexSub;
}


Complex operator-(Complex &complex_num, imaginary_t imaginary)
{
	Complex complexSub(complex_num);
	complexSub.imaginary.value -= imaginary.value;

	return complexSub;
}


Complex operator-(Complex &complex1, Complex &complex2)
{
	Complex complexSub(complex1);
	complexSub.real.value -= complex2.real.value;
	complexSub.imaginary.value -= complex2.imaginary.value;

	return complexSub;
}


Complex operator*(real_t real, Complex &complex_num)
{
	Complex complexProduct(complex_num);
	complexProduct.real.value *= real.value;
	complexProduct.imaginary.value *= real.value;

	return complexProduct;
}


Complex operator*(Complex &complex_num, real_t real)
{
	Complex complexProduct(complex_num);
	complexProduct.real.value *= real.value;
	complexProduct.imaginary.value *= real.value;

	return complexProduct;
}


Complex operator*(imaginary_t imaginary, Complex &complex_num)
{
	Complex complexProduct;
	if(imaginary.value == 0)
	{
		complexProduct.real.value = imaginary.value;
		complexProduct.imaginary.value = imaginary.value;
	}
	else
	{
		complexProduct.imaginary.value = complex_num.real.value * imaginary.value;
		complexProduct.real.value = complex_num.imaginary.value * imaginary.value * (-1);
	}

	return complexProduct;
}


Complex operator*(Complex &complex_num, imaginary_t imaginary)
{
	Complex complexProduct;
	if(imaginary.value == 0)
	{
		complexProduct.real.value = imaginary.value;
		complexProduct.imaginary.value = imaginary.value;
	}
	else
	{
		complexProduct.imaginary.value = complex_num.real.value * imaginary.value;
		complexProduct.real.value = complex_num.imaginary.value * imaginary.value * (-1);
	}

	return complexProduct;
}


Complex operator*(Complex &complex1, Complex &complex2)
{
	Complex complexProduct;

	complexProduct.real.value += (complex1.real.value * complex2.real.value);
	complexProduct.imaginary.value += (complex1.real.value * complex2.imaginary.value);
	complexProduct.imaginary.value += (complex1.imaginary.value * complex2.real.value);
	complexProduct.real.value += (complex1.imaginary.value * complex2.imaginary.value * (-1));

	return complexProduct;
}

ostream& operator<<(ostream& ost, const Complex &complex_num)
{
	ost << complex_num.real.value;

	if(complex_num.imaginary.value >= 0)
	{
		ost << "+";
	}

	ost << complex_num.imaginary.value << "i";

	return ost;
}


istream& operator>>(istream& ist, Complex &complex_num)
{

	cout << "Real part: " << endl;
	ist >> complex_num.real.value;
	cout << "Imaginary part: " << endl;
	ist >> complex_num.imaginary.value;

	return ist;
}

bool operator==(Complex &lhs, Complex &rhs)
{
	bool equal = false;

	if(lhs.real == rhs.real && lhs.imaginary == rhs.imaginary)
	{
		equal = true;
	}

	return equal;
}

