/*
 * Triangle.h
 *
 */

#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "Figure.h"

class Triangle : public Figure
{
public:
	Triangle();

	// Member function that erases drawn triangle
	virtual void erase();

	// Member function that draws triangle
	virtual void draw();

	// Member function that redraws triangle at the center
	virtual void center();
};


#endif /* TRIANGLE_H_ */
