/*
 * Circle.h
 *
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_

#include "Figure.h"

class Circle : public Figure
{
public:
	Circle();

	// Member function that erases drawn circle
	virtual void erase();

	// Member function that draws circle
	virtual void draw();

	// Member function that redraws cicle at the center
	virtual void center();
};


#endif /* CIRCLE_H_ */
