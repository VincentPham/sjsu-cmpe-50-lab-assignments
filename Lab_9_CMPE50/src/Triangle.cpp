/*
 * Triangle.cpp
 *
 */

#include <iostream>
#include "Triangle.h"

using namespace std;


Triangle::Triangle() : Figure()
{
	cout << "TRIANGLE CONSTRUCTOR" << endl;
}

void Triangle::erase()
{
	cout << "ERASE - TRIANGLE CLASS" << endl;
}

void Triangle::draw()
{
	cout << "DRAW - TRIANGLE CLASS" << endl;
}

void Triangle::center()
{
	cout << "RECENTERING TRIANGLE" << endl;
	erase();
	draw();
}








