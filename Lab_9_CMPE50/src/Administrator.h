/*
 * Administrator.h
 *
 */

#ifndef ADMINISTRATOR_H_
#define ADMINISTRATOR_H_

#include <string>
#include "SalariedEmployee.h"

using namespace std;

class Administrator: public SalariedEmployee
{
public:
	/// Constructors
	Administrator();

	Administrator(string new_name,
					string new_ssn,
					double new_weekly_salary,
					string new_admin_title,
					string new_company_responsibilities,
					string new_supervisor);

	/// Setter functions
	void set_admin_title(string new_admin_title);
	void set_company_responsibilities(string new_company_responsibilities);
	void set_supervisor(string new_supervisor);
	void read_admin_data();
	void print_check();
	void print();

private:
	string admin_title;
	string company_responsibilities;
	string supervisor;
};


#endif /* ADMINISTRATOR_H_ */
