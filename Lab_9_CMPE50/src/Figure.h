/*
 * Figure.h
 *
 */

#ifndef FIGURE_H_
#define FIGURE_H_

class Figure
{

public:

	// Constructor
	Figure();

	// Member function that erases drawn figure
	virtual void erase();

	// Member function that draws figure
	virtual void draw();

	// Member function that redraws figure at the center
	virtual void center();

};




#endif /* FIGURE_H_ */
