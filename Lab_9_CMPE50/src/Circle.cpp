/*
 * Circle.cpp
 *
 */

#include <iostream>
#include "Circle.h"

using namespace std;


Circle::Circle() : Figure()
{
	cout << "CIRCLE CONSTRUCTOR" << endl;
}

void Circle::erase()
{
	cout << "ERASE - CIRCLE CLASS" << endl;
}

void Circle::draw()
{
	cout << "DRAW - CIRCLE CLASS" << endl;
}

void Circle::center()
{
	cout << "RECENTERING CIRCLE" << endl;
	erase();
	draw();
}








