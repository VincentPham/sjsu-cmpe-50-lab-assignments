#include <iostream>
#include <string>
#include "Figure.h"

using namespace std;

Figure::Figure()
{
	cout << "FIGURE CONSTRUCTOR" << endl;
}

void Figure::erase()
{
	cout << "ERASE - FIGURE CLASS" << endl;
}

void Figure::draw()
{
	cout << "DRAW - FIGURE CLASS" << endl;
}

void Figure::center()
{
	cout << "RECENTERING FIGURE" << endl;
	erase();
	draw();
}
