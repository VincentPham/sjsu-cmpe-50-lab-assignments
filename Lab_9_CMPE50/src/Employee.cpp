/*
 * Employee.cpp
 *
 */

#include <string>
#include <cstdlib>
#include <iostream>
#include "Employee.h"
using namespace std;

///< Default constructor
Employee::Employee() :
	name("No name yet"), ssn("No number yet"), net_pay(0)
{

}

///< Constructor
Employee::Employee(string new_name, string new_ssn) :
		name(new_name), ssn(new_ssn), net_pay(0)
{

}


string Employee::get_name() const
{
	return name;
}


string Employee::get_ssn() const
{
	return ssn;
}


double Employee::get_net_pay() const
{
	return net_pay;
}


void Employee::change_name(string new_name)
{
	name = new_name;
}


void Employee::change_ssn(string new_ssn)
{
	ssn = new_ssn;
}

void Employee::set_net_pay(double new_net_pay)
{
	net_pay = new_net_pay;
}

void Employee::print_check()
{
	cout << "\nERROR: print_check FUNCTION CALLED FOR AN \n"
	     << "UNDIFFERENTIATED EMPLOYEE. Aborting the program.\n"
	     << "Check with the author of the program about this bug.\n";
	exit(1);
}

void Employee::give_raise(double amount)
{
	net_pay = amount;
}









