//============================================================================
// Name        : Lab_9_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : 
// Description : Lab 9 assignment CMPE50 @ SJSU
//============================================================================

#include <iostream>
#include <string>
#include <limits>
#include "SalariedEmployee.h"
#include "Administrator.h"
#include "Figure.h"
#include "Circle.h"
#include "Triangle.h"

using namespace std;

void exercise_1();

void exercise_2();

void myDraw(Figure *fig);

int main()
{
	//Uncomment the following line for exercise 1
	exercise_1();

	//Uncomment the following line for exercise 2
	exercise_2();
}

void exercise_1()
{
	Administrator admin1;
	int user_input = 0;
	string new_supervisor;
	string new_admin_title;

	enum  selection{default_, submit_data, set_supervisor, set_title, print_data, print_check, exit_};

	while(user_input != exit_)
	{
		cout << "Please make a selection." << endl;
		cout << "1. Submit admin data" << endl;
		cout << "2. Set admin supervisor" << endl;
		cout << "3. Set admin title" << endl;
		cout << "4. Print admin data" << endl;
		cout << "5. Print admin check" << endl;
		cout << "6. exit" << endl;

		cin >> user_input;
		cin.ignore(numeric_limits<streamsize>::max(), '\n');

		switch(user_input)
		{
			case submit_data:
				admin1.read_admin_data();
				break;

			case set_supervisor:
				cout << "Please enter in admin's new supervisor." << endl;
				getline(cin, new_supervisor);
				admin1.set_supervisor(new_supervisor);
				break;

			case set_title:
				cout << "Please enter in admin title." << endl;
				getline(cin, new_admin_title);
				admin1.set_admin_title(new_admin_title);
				break;

			case print_data:
				admin1.print();
				break;

			case print_check:
				admin1.print_check();
				break;

			default:
				break;
		}

	}
}

void exercise_2()
{
	Figure *fig;
	Triangle *tri = new Triangle;

	fig = tri;
	fig->draw();
	cout << "\n Derived class Triangle object calling center(). \n";
	fig->center();

	myDraw(tri);

	Circle *cir = new Circle;
	fig = cir;
	cir->draw();
	cout << "\n Derived class Circle object calling center(). \n";
	cir->center();

	myDraw(cir);
}

void myDraw(Figure *fig)
{
	fig->draw();
	cout << "\n myDraw: Derived class object calling center(). \n";
	fig->center();
}




