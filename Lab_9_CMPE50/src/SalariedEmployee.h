/*
 * SalariedEmployee.h
 *
 */

#ifndef SALARIEDEMPLOYEE_H_
#define SALARIEDEMPLOYEE_H_
#include <cstdlib>
#include <string>
#include <iostream>

#include "Employee.h"

using namespace std;

class SalariedEmployee:public Employee
{
public:
	SalariedEmployee();
	SalariedEmployee (string new_name, string new_ssn, double new_weekly_salary);
	double get_salary();
	void change_salary(double new_salary);
	void print_check();
	void give_raise(double amount);
protected:
	double weekly_salary;    ///< Weekly salary of salaried employee
};



#endif /* SALARIEDEMPLOYEE_H_ */
