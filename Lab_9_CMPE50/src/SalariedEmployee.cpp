/*
 * SalariedEmployee.cpp
 *
 */

#include "SalariedEmployee.h"

using namespace std;

///< Salaried Employee default constructor
SalariedEmployee::SalariedEmployee() :
		Employee(), weekly_salary(0)
{

}


///< Salaried Employee constructor
SalariedEmployee::SalariedEmployee (string new_name, string new_ssn, double new_weekly_salary) :
		Employee(new_name, new_ssn), weekly_salary(new_weekly_salary)
{

}


double SalariedEmployee::get_salary()
{
	return weekly_salary;
}


void SalariedEmployee::change_salary(double new_salary)
{
	weekly_salary = new_salary;
	net_pay = new_salary * 52;
}


void SalariedEmployee::print_check()
{
	set_net_pay(weekly_salary);
	cout << "\n_____________________________________________\n";
	cout << "Pay to the order of " << get_name( ) << endl;
	cout << "The sum of " << get_net_pay( ) << " Dollars\n";
	cout << "________________________________________________\n";
	cout << "Check Stub NOT NEGOTIABLE \n";
	cout << "Employee Number: " << get_ssn( ) << endl;
	cout << "Salaried Employee. Regular Pay: "
	<< weekly_salary << endl;
	cout << "______________________________________________\n";
}


void SalariedEmployee::give_raise(double amount)
{
	net_pay += (net_pay * amount);
	weekly_salary = net_pay / 52;
}


