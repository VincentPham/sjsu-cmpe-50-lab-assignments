#ifndef EMPLOYEE_H_INCLUDED
#define EMPLOYEE_H_INCLUDED

#include <string>
using namespace std;


class Employee
{
public:
	/// Constructors
	Employee();
	Employee(string new_name, string new_ssn);

	/// Getter functions
	string get_name() const;
	string get_ssn() const;
	double get_net_pay() const;

	/// Setter functions
	void change_name(string new_name);
	void change_ssn(string new_ssn);
	void set_net_pay(double new_net_pay);
	void give_raise(double amount);

	/// Output function
	void print_check();

protected:
	string name;       ///< Name of employee
	string ssn;        ///< Social security number of employee
	double net_pay;    ///< Annual net pay of employee
};

#endif // EMPLOYEE_H_INCLUDED
