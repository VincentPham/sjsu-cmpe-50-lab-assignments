/*
 * Administrator.cpp
 *
 */

#include "Administrator.h"
#include <limits>

Administrator::Administrator(): SalariedEmployee()
{

}


Administrator::Administrator(string new_name,
				string new_ssn,
				double new_weekly_salary,
				string new_admin_title,
				string new_company_responsibilities,
				string new_supervisor):
						SalariedEmployee(new_name, new_ssn, new_weekly_salary),
						admin_title(new_admin_title),
						company_responsibilities(new_company_responsibilities),
						supervisor(new_supervisor)
{
	net_pay = weekly_salary * 52;
}

void Administrator::set_admin_title(string new_admin_title)
{
	admin_title = new_admin_title;
}

void Administrator::set_company_responsibilities(string new_company_responsibilities)
{
	company_responsibilities = new_company_responsibilities;
}
void Administrator::set_supervisor(string new_supervisor)
{
	supervisor = new_supervisor;
}

void Administrator::read_admin_data()
{
	cout << "Enter in the following information. " << endl;

	cout << "Please enter administrator name" << endl;
	getline(cin, name);

	cout << "Please enter social-security number." << endl;
	getline(cin, ssn);

	cout << "Please enter in weekly salary." << endl;
	cin >> weekly_salary;
	cin.ignore(numeric_limits<streamsize>::max(), '\n');
	net_pay = 52 * weekly_salary;

	cout << "Please enter in administrator title (Director, Vice President, etc)." << endl;
	getline(cin, admin_title);

	cout << "Please enter in administrator company responsibilities (Productions, Accounting, Personnel, etc)." << endl;
	getline(cin, company_responsibilities);

	cout << "Please enter in administrator's supervisor." << endl;
	getline(cin, supervisor);

}


void Administrator::print_check()
{
	cout << "\n_____________________________________________\n";
	cout << "Pay to the order of " << get_name( ) << endl;
	cout << "The sum of " << get_salary() << " Dollars\n";
	cout << "________________________________________________\n";
	cout << "Check Stub NOT NEGOTIABLE \n";
	cout << "Employee Number: " << get_ssn( ) << endl;
	cout << "Administrator Salaried Employee. Regular Pay: "
	<< weekly_salary << endl;
	cout << "______________________________________________\n";
}

void Administrator::print()
{
	cout << "************Administrator Information************" << endl;
	cout << "Name: " << name << endl;
	cout << "Social Security #: " << ssn << endl;
	cout << "Net pay: " <<  "$" << net_pay << endl;
	cout << "Weekly Salary: " <<  "$" <<weekly_salary << endl;
	cout << "Title: " << admin_title << endl;
	cout << "Company Responsibilities: " << company_responsibilities << endl;
	cout << "Supervisor: " << supervisor << endl;
	cout << "*************************************************" << endl;
}


