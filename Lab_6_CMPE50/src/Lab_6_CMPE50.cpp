//============================================================================
// Name        : Lab_6_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Lab 6 - CMPE50 - Classes Part 2, Friend Functions
//============================================================================

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstdlib>
#include <cctype>

using namespace std;

/// Resource class used in exercise 1
class Resource
{
public:
	//Constructors
	Resource();
	Resource(int write);
	//Member Functions
	int get_status();
	int get_writeTo();
	void set_status(int set_stat);
	void set_writeTo(int set_write);
	void output(ostream &out_stream);
	friend bool check_status(Resource &res1, Resource &res2);

private:
	int status, writeTo;
};

/// Rational class used in exercise 2
class Rational
{
public:
	//Constructors
	Rational();
	Rational(int num, int denom);
	void input(istream& in);
	void output(ostream& out);
	friend Rational add(Rational& num1, Rational& num2);
	friend Rational subtract(Rational& num1, Rational& num2);
	friend Rational multiply(Rational& num1, Rational& num2);
	friend Rational divide(Rational& num1, Rational& num2);
	friend bool equal(const Rational& num1, const Rational& num2);
	friend bool less_than(const Rational& num1, const Rational& num2);
	friend bool greater_than(const Rational& num1, const Rational& num2);
	friend void normalize(Rational& num);

private:
	int numerator, denominator;

};

int gcd (int n1, int n2); ///< Function that finds the least greatest common denominator
void exercise_1(); ///< Function prototype that demonstrates exercise 1
void exercise_2(); ///< Function prototype that demonstrates exercise 2
void exercise2_io(ostream& output, Rational& number1, Rational& number2);

int main()
{
//Uncomment the following line for exericse 1
	//exercise_1();
//Uncomment the following line for exercise 2
	exercise_2();
}


/// Lab Exercise 1
void exercise_1()
{
	Resource res1(10), res2;
	cout << res1.get_status() << endl;
	cout << res1.get_writeTo() << endl;
	cout << res2.get_status() << endl;
	cout << res2.get_writeTo() << endl;

	res2.set_status(1);
	res2.set_writeTo(1);

	cout << res1.get_status() << endl;
	cout << res1.get_writeTo() << endl;
	cout << res2.get_status() << endl;
	cout << res2.get_writeTo() << endl;

	if(check_status(res1,res2))
	{

	}

	if(check_status(res1,res2))
	{
		cout << "Statement is true!" << endl;
	}
}

/// Lab Exercise 2
void exercise_2()
{
	char entry = 0;
	char file_name[50] = {0};
	Rational number1, number2(1, -4);


	/// File I/O
	do{
		cout << "Would you like to work from your keyboard or a file?" << endl;
		cout << "Press 'k' for keyboard or 'f' for file." << endl;
		cin >> entry;
		if(entry == 'f')
		{
			/// File stream objects
			ifstream in_stream;
			ofstream out_stream;

			/// User prompt
			cout << "You have chosen to input and output from and to a file." << endl;
			cout << "Please enter the file name you wish to read from along with its extension." << endl;
			cin >> file_name;
			cout << "The file name you have entered was " << file_name << endl;

			in_stream.open(file_name); ///< Open file for reading
			if(in_stream.fail())
			{
				cout << "Input file opening failed. \n";
				exit(1);
			}

			number1.input(in_stream); ///< Read from input file

			///User prompt
			cout << "Please enter the file name you wish to write to, along with its extension." << endl;
			cin >> file_name;
			cout << "The file name you have entered was " << file_name << endl;
			out_stream.open(file_name); ///< Open file for writing
			if(out_stream.fail())
			{
				cout << "Output file opening failed. \n";
				exit(1);
			}

			exercise2_io(out_stream, number1, number2);
			out_stream.close();
			cout << "The output file has finished generating." << endl;
		}

		if(entry == 'k')
		{
			cout << "You have chosen to input and output from and to the keyboard." << endl;
			cout << "Please enter in the numerator and denominator, respectively." << endl;
			number1.input(cin); ///< Get inputs from user input
			exercise2_io(cout, number1, number2);
		}
	}while(entry != 'k' || entry != 'f');
}

/// Output for exercise 2
void exercise2_io(ostream& output, Rational& number1, Rational& number2)
{
	Rational solution;
	output << "Number 1: ";
	number1.output(output);
	output << "Number 2: ";
	number2.output(output);

	//Add
	solution = add(number1, number2);
	output << "Sum of number 1 and 2 is ";
	normalize(solution);
	solution.output(output);

	//Subtract
	solution = subtract(number1, number2);
	output << "Subtraction of number 2 from 1 is ";
	normalize(solution);
	solution.output(output);

	//Multiply
	solution = multiply(number1, number2);
	output << "Product of number 1 and 2 is ";
	normalize(solution);
	solution.output(output);

	//Divide
	solution = divide(number1, number2);
	output << "Division of number 1 and 2 is ";
	normalize(solution);
	solution.output(output);

	normalize(number1);
	normalize(number2);

	if(equal(number1, number2))
	{
		output << "Number 1 and 2 are equal!" << endl;
	}
	else if(less_than(number1, number2))
	{
		output << "Number 1 is less than number 2." << endl;
	}
	else if(greater_than(number1, number2))
	{
		output << "Number 1 is greater than number 2." << endl;
	}
}

/// Default constructor
Resource::Resource()
{
	status = 0;
	writeTo = 0;
}

/// Parameterized constructor
Resource::Resource(int write)
{
	status = 0;
	writeTo = write;
}

/// Accessor function that gets status object
int Resource::get_status()
{
	return status;
}

/// Accessor function that gets writeTo object
int Resource::get_writeTo()
{
	return writeTo;
}

/// Mutator function that sets status
void Resource::set_status(int set_stat)
{
	status = set_stat;
}

/// Mutator function that sets writeTo
void Resource::set_writeTo(int set_write)
{
	writeTo = set_write;
}

void Resource::output(ostream &out_stream)
{
	out_stream << status;
}

bool check_status(Resource &res1, Resource &res2)
{
	if(res1.status == 1 && res2.status == 1)
	{
		cout << "Status Available" << endl;
		return true;
	}
	else
	{
		cout << "Resource Available" << endl;
		return false;
	}
}

/// Default constructor
Rational::Rational()
{
	numerator = 0;
	denominator = 1;
}

/// Parameterized constructor
Rational::Rational(int num, int denom)
{
	numerator = num;
	denominator = denom;
}

/// Mutator function that takes a rational number input
void Rational::input(istream& in)
{
	in >> numerator >> denominator;
	while(denominator == 0)
	{
		cout << "Denominator value of '0' is invalid!" << endl;
		cout << "Please enter a new value." << endl;
		in >> denominator;
	};
}

/// Accessor function that outputs a rational number
void Rational::output(ostream& out)
{
	out << numerator << "/" << denominator << "." << endl;
}

/// Finds the sum of number 1 and number 2
Rational add(Rational& num1, Rational& num2)
{
	Rational sum, frac1, frac2;
	frac1.numerator = num1.numerator * num2.denominator;
	frac2.numerator = num2.numerator * num1.denominator;
	sum.denominator = num1.denominator * num2.denominator;
	sum.numerator = frac1.numerator + frac2.numerator;
	return sum;
}

/// Subtracts number 2 from number 1
Rational subtract(Rational& num1, Rational& num2)
{
	Rational sub, frac1, frac2;
	frac1.numerator = num1.numerator * num2.denominator;
	frac2.numerator = num2.numerator * num1.denominator;
	sub.denominator = num1.denominator * num2.denominator;
	sub.numerator = frac1.numerator - frac2.numerator;
	return sub;
}

/// Multiplies number 1 and 2 and returns the product
Rational multiply(Rational& num1, Rational& num2)
{
	Rational product;
	product.denominator = num1.denominator * num2.denominator;
	product.numerator = num1.numerator * num2.numerator;
	return product;
}

/// Divides number 1 and number 2, and returns the quotient
Rational divide(Rational& num1, Rational& num2)
{
	Rational quotient, temp;
	quotient.numerator = num1.numerator * num2.denominator;
	quotient.denominator = num1.denominator * num2.numerator;
	return quotient;
}

/// Determines if number 1 and nubmer 2 are equal
bool equal(const Rational& num1, const Rational& num2)
{
	if (num1.numerator == num2.numerator && num1.denominator == num2.denominator)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/// Determines if number 1 is less than number 2
bool less_than(const Rational& num1, const Rational& num2)
{
	int a = 0, b = 0;
	a = num1.numerator * num2.denominator;
	b = num2.numerator * num1.denominator;

	if (a < b)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/// Determines if number 1 is greater than number 2
bool greater_than(const Rational& num1, const Rational& num2)
{
	int a = 0, b = 0;
	a = num1.numerator * num2.denominator;
	b = num2.numerator * num1.denominator;

	if (a > b)
	{
		return true;
	}
	else
	{
		return false;
	}
}

/// Simplifies the rational number
void normalize(Rational& num)
{
	int gcf = 0;
	gcf = gcd(num.numerator, num.denominator);
	num.numerator /= gcf;
	num.denominator /= gcf;
	if(num.denominator < 0)
	{
		num.denominator *= (-1);
		num.numerator *= (-1);
	}
}

/// Finds the greatest common denominator
int gcd (int n1, int n2)
{
	if(n1 == 0)
	{
		return n2;
	}
	return gcd(n2 % n1, n1);
}
