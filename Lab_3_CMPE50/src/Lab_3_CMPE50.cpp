//============================================================================
// Name        : Lab_3_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Lab 3 - CMPE50 - File I/O and I/O formatting
//============================================================================

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cctype>
#include <iomanip>
using namespace std;

void exercise_1(void);
void exercise_2(void);
void exercise_3(void);

void selection_sort(int arr[], int numbers_used);
int find_smallest(int arr[],int present_index, int numbers_used);
void swap(int& x, int &y);
void new_line(void);
int count_occurence(int arr[], int arr_N[], int arr_count[], int numbers_used);
void file_merge(ifstream& input_stream1, ifstream& input_stream2, ofstream& output_stream);
int main()
{

//Uncomment the following line for Exercise 1
	//exercise_1();

//Uncomment the following line for Exercise 2
	//exercise_2();

//Uncomment the following line for Exercise 3
	exercise_3();

	return 0;
}

void exercise_1(void)
{
	//Declare streams
	ifstream in_stream;
	ofstream out_stream;

	char input_arr[100] = {0}; //stores values from file
	int count = 0;

	//Opens intput file
	in_stream.open("LabInput3-1.txt");
	if(in_stream.fail())
	{
		cout << "Input file opening failed. \n";
		exit(1);
	}
	//Reads entire file and stores in input_arr[]
	while(in_stream >> input_arr[count++])
	{

	}
	in_stream.close(); //Closes input file

	//Capitalizes entire array
	for(int i = 0; i < 100; i++)
	{
		input_arr[i] = toupper(input_arr[i]);
	}
	//Opens output stream
	out_stream.open("LabOutput3-1.txt");
	if(out_stream.fail())
	{
		cout << "Output file opening failed. \n";
		exit(1);
	}
	//Outputs capitalized array to file "LabOutput3-1.txt"
	out_stream << input_arr;
	out_stream.close(); //Closes output stream
}

void exercise_2(void)
{

	int number_list[50] = {0};
	int list_size = 0;

	int N_array[50] = {0};
	int occurence_size = 0;
	int count_array[50] = {0};

	cout << "Please specify if you want to manually enter in a list of numbers \n"
		 <<"or if you would like to enter in the numbers via file \n"
		 <<"For manual entry, enter 'm'. For file entry, enter 'f'." << endl;

	char user_decision;

	while(1)
	{
		cin >> user_decision;
		new_line(); //captures characters until '\n' is reached if multiple characters entered
		if(user_decision != 'm' && user_decision != 'f')
		{
			cout << "You have entered an invalid entry, please re-enter your decision. \n";
		}
		else
		{
			break;
		}
	}

	//MANUAL INPUT
	if(user_decision == 'm')
	{

		cout << "You have chosen to enter in the list of numbers manually.\n";
		cout << "Please enter the size of the list you are entering.\n";
		cin >> list_size;
		for(int i = 0; i < list_size; i++)
		{
			cout << "Please enter in entry " << i+1 << "." << endl;
			cin >> number_list[i];
		}

		selection_sort(number_list, list_size);
		occurence_size = count_occurence(number_list, N_array, count_array, list_size);

		cout << "N";
		cout << "        ";
		cout << "Count" << endl;

		for(int k = 0; k < occurence_size; k++)
		{
			cout << N_array[k];
			if (N_array[k] < 0 || N_array[k] >= 100)
			{
				cout << "       ";
			}
			else
			{
				cout << "        ";
			}

			cout << count_array[k] << endl << endl;
		}

		cout << "The program has finished!" << endl;

	}

	//FILE INPUT
	if(user_decision == 'f')
	{
		char file_name[50] = {0};
		cout << "You have chosen to enter in the list via a file. \n";
		cout << "Please enter in the file name and extension you wish to read from. \n";
		cin >> file_name;

		//Declare streams
		ifstream in_stream;
		ofstream out_stream;

		//Open input file stream
		in_stream.open(file_name);
		if(in_stream.fail())
		{
			cout << "Input file opening failed. \n";
			exit(1);
		}

		//Reads entire file and stores in number_list[]
		while(in_stream >> number_list[list_size++])
		{

		}

		in_stream.close();
		list_size--;
		selection_sort(number_list, list_size);
		occurence_size = count_occurence(number_list, N_array, count_array, list_size);

		out_stream.open("LabOutput3-2.txt");

		if(out_stream.fail())
		{
			cout << "Output file opening failed. \n";
			exit(1);
		}

		out_stream << "N";
		out_stream << "        ";
		out_stream << "Count" << endl;

		for(int k = 0; k < occurence_size; k++)
		{
			out_stream << N_array[k];
			if (N_array[k] < 0 || N_array[k] >= 100)
			{
				out_stream << "       ";
			}
			else
			{
				out_stream << "        ";
			}

			out_stream << count_array[k] << endl;
		}

		out_stream.close();
		cout << "The file has been processed, and data has been" << endl
			 << "ouput to the file \"LabOutput3-2.txt\"" << endl;
	}
}

//Counts number of times number occurs in list
int count_occurence(int arr[], int arr_N[], int arr_count[], int numbers_used)
{
	int index = 0;
	int count = 1;

	for(int i = 0; i < numbers_used; i++)
	{
		arr_N[index] = arr[i];
		arr_count[i] = count;
		if(arr[i] == arr[i+1])
		{
			count++;
			arr_count[index] = count;
		}
		else
		{
			index++;
			count = 1;
			arr_count[index] = count;
		}
	}

	return index;
}

void exercise_3(void)
{
	ifstream input_file1, input_file2;
	ofstream output_file;

	input_file1.open("LabInput3-3-1.txt");
	if(input_file1.fail())
	{
		cout << "Input file 1 has failed to open." << endl;
		exit(1);
	}

	input_file2.open("LabInput3-3-2.txt");
	if(input_file2.fail())
	{
		cout << "Input file 2 has failed to open." << endl;
		exit(1);
	}

	output_file.open("LabOutput3-3.txt");
	if(output_file.fail())
	{
		cout << "Output file has failed to open." << endl;
		exit(1);
	}

	file_merge(input_file1, input_file2, output_file);
	input_file1.close();
	input_file2.close();
	output_file.close();
}

void file_merge(ifstream& input_stream1, ifstream& input_stream2, ofstream& output_stream)
{
	int arr_numbers[100] = {0};
	int list_size = 0;

	while(!input_stream1.eof())
	{
		input_stream1 >> arr_numbers[list_size++];
	}

	while(!input_stream2.eof())
	{
		input_stream2 >> arr_numbers[list_size++];
	}

	selection_sort(arr_numbers, list_size);

	for(int index = 0; index < list_size; index++)
	{
		output_stream << arr_numbers[index] << endl;
	}

}

//Input Checking, discards excess entries
void new_line()
{
	char garbage;
	do
	{
		cin.get(garbage);
	}while(garbage != '\n');
}

//Sorts list
void selection_sort(int arr[], int numbers_used)
{
	int min_index = 0;

	for(int index = 0; index < numbers_used - 1; index++)
	{
		//Finding the smallest number in the array
		min_index = find_smallest(arr, index, numbers_used);
	    swap(arr[min_index], arr[index]);
	}
}

int find_smallest(int arr[], int present_index, int numbers_used)
{
	int smallest_number = arr[present_index];
	int min_index  = present_index;

	for(int i = present_index + 1; i < numbers_used; i++)
	{
		if(smallest_number > arr[i])
		{
			smallest_number = arr[i];
			min_index = i;
		}
	}

	return min_index;
}

void swap(int& x, int &y)
{
	int temp = x;
	x = y;
	y = temp;
}
