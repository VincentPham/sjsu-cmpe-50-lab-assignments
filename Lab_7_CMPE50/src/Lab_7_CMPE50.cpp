#include <stdio.h>
#include <string>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cctype>
#include <istream>

using namespace std;

void exercise_1();
void print_seats(int num_rows, int num_columns,char* _seat[], int _row[]);
int col_2_index(char col);
void printField (string str, char entry);
void exercise_2();
char* delete_repeats(char str[], int size);

int main()
{
	//Uncomment the following line for exercise 1
	//exercise_1();

	//Uncomment the following line for exercise 2
	exercise_2();
	return 0;
}

void exercise_1()
{
	int rows = 0;         ///< Number of rows on plane
	int columns = 4;      ///< Number of columns on plane
    int selected_row;     ///< User selected row
	char selected_column; ///<User selected column
	char entry = 0;       ///< buffer to hold keyboard/file read selection

	istream *inStream;
	ifstream in_stream;

	/// File I/O
	do{
		cout << "Would you like to select seats from your keyboard or a file?" << endl;
		cout << "Press 'k' for keyboard or 'f' for file." << endl;

		if(cin >> entry)
		{
			cout << "Entry: " << entry << endl;
		}
		else
		{
			cout << "An error has occurred! Please try again." << endl;
			cin.clear();
			cin.ignore(1, '\n');
		}

		/*if(entry != 'k' && entry != 'f')
		{
			cout << "Invalid selection has been made. Please try again." << endl;
		}*/

	}while(entry != 'k' && entry != 'f');

	if(entry == 'k')
	{
		cout << "You have chosen to select seats from your keyboard." << endl;
		inStream = &cin;
	}
	else if(entry == 'f')
	{
		char file_name[50];

		cout << "You have chosen to select seats from a file." << endl;
		cout << "Please enter in the file you want to read from, along with its extension (i.e. File.txt)." << endl;

		while(true)
		{
			if(cin >> file_name)
			{
				///valid read
				cout << "FILE NAME: " << file_name << endl;
				break;
			}
			else
			{
				cout << "Unable to read user input! Please try again." << endl;
				cin.clear();
				cin.ignore(1, '\n');
			}
		}

		in_stream.open(file_name);
		if(in_stream.fail())
		{
			cout << "Input file opening failed. \n";
			exit(1);
		}

		inStream = &in_stream;
	}
	else
	{
		cout << "ERROR: an error has occurred!" << endl;
		exit(1);
	}

	/// Prompt user for number of rows on plane
	cout << "Please enter how many rows are on the airplane." << endl;
	while(true)
	{
		if(*inStream >> rows)
		{
			cout << "ROWS: " << rows << endl;
			break;
		}

		cout << "Invalid input, please try again." << endl;
		inStream->clear();
		inStream->ignore(1, '\n');
	}

	/// Dynamically allocate column containing row numbers
	int* row_arr = new int [rows];

    typedef char* pSeat; ///< Type define pSeat to be a char pointer

    /// Allocates memory for a 2D array
    pSeat *seat = new pSeat [rows];
    for(int row = 0; row < rows; row++)
    {
    	row_arr[row] = row + 1; ///< Assign row number to initial column

    	seat[row] = new char [columns]; ///< Create dynamic array for each row

    	///< Assign columns with an alphabetical value (i.e. A-D)
    	for(int column = 0; column < columns; column++)
    	{
    		seat[row][column] = 65 + column;
    	}
    }

    print_seats(rows, columns, seat, row_arr);

    bool plane_full = false; ///< Boolean value that indicates if plane is full
    int max_seats = rows * (columns); ///< Max number of seats available
    int seats_occupied = 0; ///< Number of seats occupied
    bool validCol = false; ///< Boolean value that indicates if a valid column was entered
    bool validRow = false; ///< Boolean value that indicates if a valid row was entered
    bool selected_seat = false; ///< Boolean value that indicates if a valid seat was selected

    while(!plane_full)
    {
    	selected_seat = false;

		cout << "Please enter the seat row and column you would like." << endl;


		while(!selected_seat)
		{
			/// Get ROW
			validRow = false;
			while(!validRow)
			{
				printField("row", entry);
				if(*inStream >> setw(0) >> selected_row)
				{
					/// Valid input
				}
				else
				{
					cout << "Unable to read user input! Please try again." << endl;
					inStream->clear();
					inStream->ignore(1, '\n');
				}

				if(selected_row >= 1 && selected_row <= rows)
				{
					validRow = true;
				}
				else
				{
					cout << "The row you have entered is invalid! Please try again." << endl;
				}
			}

			cout << endl;

			///Get COLUMN
			validCol = false;
			while(!validCol)
			{
				printField("column", entry);

				if(*inStream >> selected_column)
				{

				}
				else
				{
					cout << "Unable to read user input! Please try again." << endl;
					inStream->clear();
					inStream->ignore(1, '\n');
				}

				selected_column = toupper(selected_column);

				if(selected_column >= 65 && selected_column <= 68)
				{
					validCol = true;
				}
				else
				{
					cout << "The column you have entered is invalid! Please try again." << endl;
				}
			}

			cout << endl;
			cout << "SELECTED SEAT: " << selected_row << selected_column << endl << endl;;

			int selCol_ind = col_2_index(selected_column);
			if(seat[selected_row - 1][selCol_ind] == 'X')
			{
				cout << "This seat has already been selected. Please select another seat." << endl;
			}
			else
			{
				seat[selected_row - 1][selCol_ind] = 'X';
				seats_occupied++;
				selected_seat = true;
			}
		}

		print_seats(rows, columns, seat, row_arr);

		if(seats_occupied == max_seats)
		{
			plane_full = true;

			cout << "The plane is FULL. No more seats available." << endl;
		}
    }

    if(entry == 'f')
    {
    	in_stream.close();
    }

    /// Deallocating memory of 2D array
    for(int row = 0; row < rows; row++)
    {
    	delete [] seat[row];
    }

    delete [] seat;

}

void exercise_2()
{
	char str[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ ABCDEFGHIJKLMNOPQRSTUVWXYZ ABCDEFGHIJKLMNOPQRSTUVWXYZ ABCDEFGHIJKLMNOPQRSTUVWXYZ";

	int sze = sizeof(str)/sizeof(char);		///< Gets length of array, can also use strlen()
	char *new_str = delete_repeats(str, sze);
	cout << new_str << endl;
	delete [] new_str;
}

int col_2_index(char col)
{
    if(col == 'A')
    {
    	return 0;
    }

    else if(col == 'B')
    {
    	return 1;
    }

    else if(col == 'C')
    {
    	return 2;
    }

    else if(col == 'D')
    {
    	return 3;
    }

    return 0;
}


void print_seats(int num_rows, int num_columns, char* _seat[], int _row[])
{
    /// Prints rows
    for(int row = 0; row < num_rows; row++)
    {
    	cout << _row[row] << " ";
    	for(int column = 0; column < num_columns; column++)
    	{
    		cout << _seat[row][column] << " ";
    	}

    	cout << endl;
    }
}

void printField (string str, char entry)
{
	if(entry == 'k')
	{
		if(str == "row")
		{
			cout << "Row: ";
		}
		else if (str == "column")
		{
			cout << "Column: ";
		}
	}
}

///< Function used in exercise 2 to remove repeated characters and returns a new dynamic array.
char* delete_repeats(char str[], int size)
{
	int new_size = size;	///< Length of new string

	/// Loops through entire string and fills repeated characters with 0x00 and tracks length of new string
	for(int index = 0; index < size-1; index++)
	{
		if(str[index] != 0x00)
		{
			for(int pos = index + 1; pos < size-1; pos++)
			{
				if(str[index] == str[pos])
				{
					str[pos] = 0x00;
					new_size--;
				}
			}
		}
	}


	char *new_arr = new char [new_size];	///< Allocate memory for new string.
	int ind = 0;	///< Iterator for new string.

	/// Loops through old string and copies valid values into new string
	for(int itr = 0; itr < size - 1; itr++)
	{
		if(str[itr] != 0x00)
		{
			new_arr[ind++] = str[itr];
		}
	}

	new_arr[new_size - 1] = '\0';

	return new_arr;

}

