//============================================================================
// Name        : Lab_1_CMPE50.cpp
// Author      : Vincent Pham
// Version     :
// Copyright   : Your copyright notice
// Description : Lab 1, Primality Test
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int var = 0;
	bool prime_flag = true;
	cout << "Please enter in a number that is greater than 2." << endl;

	//Get input
	while(1)
	{
		cin >> var;
		if(var < 2)
		{
			cout << "Please enter in a valid input." << endl;
			cout <<"The number you have entered is not greater than or equal to 2." << endl;
		}
		else
		{
			break;
		}
	}

	cout << "The number you entered was " << var << "." << endl;
	cout << "The square of the number is " << var*var << "." <<endl;
	cout << "We will now find all the prime numbers between 2 and " << var << endl;

	//Finds prime numbers
	if(var == 2 || var == 3)
	{
		cout << "The number you have entered is prime!" << endl;
	}
	else
	{
		for(int i = 3; i < var; i++)
		{
			for(int j = 2; j <= i - 1 ; j++)
			{
				if(i % j == 0)
				{
					prime_flag = false;

				}
			}

			if(prime_flag == true)
			{
				cout << i << endl;
			}

			prime_flag = true;
		}
	}

	return 0;
}
